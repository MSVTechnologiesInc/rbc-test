import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { IUser } from './user.interface';
// import { User } from './user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  users: IUser[];
  currentUser: IUser;

  constructor(private usersService: UsersService) {}

  ngOnInit(): void {
    this.usersService.getUsers().subscribe(receivedUsers => {
      this.users = receivedUsers;
      // this.currentUser = this.users[0];
      this.currentUser = this.users.find(u => u.id === 1);
    });
  }

  // changeCurrentUser(event: MouseEvent, user: IUser) {
  //   this.currentUser = user;
  // }

  public onChange(userId: number): void {
    const currentUser = this.users.find(u => u.id === +userId);
    this.currentUser = currentUser;
  }
}
