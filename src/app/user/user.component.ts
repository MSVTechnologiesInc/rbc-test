import { ValidationPatterns } from './../validation.patterns';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
// import { User } from '../user.model';
// import {
//   FormGroup,
//   FormBuilder,
//   FormControl,
//   Validators,
//   AbstractControl
// } from '@angular/forms';
import { IUser, IAddress } from '../user.interface';
import { UsersService } from '../users.service';
import {
  FormGroup,
  FormBuilder,
  AbstractControl
} from '../ngx-strongly-typed-forms/model';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnChanges {
  @Input() user: IUser;
  userForm: FormGroup<IUser>;

  // private _user: IUser;
  // get user(): IUser {
  //   return this._user;
  // }
  // @Input()
  // set user(newUser: IUser) {
  //   this._user = newUser;
  //   this.buildUserForm(newUser);
  // }

  // set setUser(newUser: IUser) {
  //   this.buildUserForm(newUser);
  // }

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService
  ) {}

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.user && changes.user.currentValue) {
      this.buildUserForm(changes.user.currentValue);
    }
  }


  asyncValidatorCheckUniqueCity = (
    control: AbstractControl<string>
  ) => {
    return this.usersService
      .getUsers()
      .toPromise()
      .then(
        (response: IUser[]) =>
          response.find(u => u.address.city === control.value)
            ? {
                uniqueCity: {
                  message: 'The City is not unique'
                }
              }
            : null
      );
  }

  buildUserForm(user: IUser) {
    // debugger;
    this.userForm = this.formBuilder.group<IUser>({
      id: [user.id],

      name: [
        user.name,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(256)
        ]
      ],
      email: [
        user.email,
        [
          Validators.required,
          Validators.email
          // , Validators.pattern(ValidationPatterns.email)
        ]
      ],
      // phone: [
      //   user.phone,
      //   Validators.compose([
      //     Validators.required,
      //     Validators.pattern(ValidationPatterns.phone)
      //   ])
      // ],
      address: this.formBuilder.group<IAddress>({
        city: [
          user.address.city,
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(256)
          ],
          this.asyncValidatorCheckUniqueCity
        ]
      })
    });
  }
}
