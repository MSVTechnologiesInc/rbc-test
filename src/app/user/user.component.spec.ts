// import { User } from './../user.model';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './user.component';
import { IUser } from '../user.interface';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UsersService } from '../users.service';
import { FormBuilder, FormControl } from '../ngx-strongly-typed-forms/model';
import { By } from '@angular/platform-browser';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      providers: [UsersService, FormBuilder]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display user name', async(() => {
    // given
    // const compiled = fixture.debugElement.nativeElement;
    // const compiled = fixture.debugElement.query(By.css('#name'));

    // when
    // fixture.componentInstance.user = new User('Sergey Malykh');
    const newUser: IUser = <IUser>{
      name: 'Sergey Malykh',
      email: 'serhiy.malykh@gmail.com',
      address: { city: 'Toronto' }
    };
    component.user = newUser;
    fixture.detectChanges(); // SergeyM - MUST trigger initial data binding

    console.log(fixture.debugElement.componentInstance);
    console.log(fixture.debugElement.nativeElement);
    console.log(fixture.debugElement.query(By.css('#name')));

    // then
    // expect(compiled.querySelectorAll('#name').value).toMatch('Sergey Malykh');

    // SergeyM: Sorry, the content of 'fixture.debugElement.nativeElement' is under binding, and as result - empty
    // expect(fixture.debugElement.query(By.css('#name'))).toMatch('Sergey Malykh');

    expect(fixture.debugElement.componentInstance.user.name).toMatch(
      'Sergey Malykh'
    );
  }));

  it('should city be unique', fakeAsync(() => {
    // given
    const abstractControl = new FormControl<string>('WisokyburghXX');
    const asyncValidatorCheckUniqueCity =  fixture.componentInstance.asyncValidatorCheckUniqueCity(abstractControl);
    tick();
    fixture.detectChanges();
    console.log(asyncValidatorCheckUniqueCity);
    // expect(asyncValidatorCheckUniqueCity).toBe(null);
    asyncValidatorCheckUniqueCity.then(result => {
      // fixture.detectChanges();
      console.log(result);
      expect(result).toBe(null);
    });
  }));
});
